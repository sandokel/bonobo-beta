class MediaLibrary < ActiveRecord::Base
	mount_uploader :file_name, MediaUploader

	has_many :lesson_resources
	has_many :lessons, through: :lesson_resources

	validates :link, uniqueness: { scope: :user_id, message: "That link has been already added.", allow_blank: true }

	def name
		if file_name.url
			file_name.url.split("/").last
		else
			link
		end
  	end

end
