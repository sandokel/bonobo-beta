class Course < ActiveRecord::Base
	has_one :category
	has_one :level
	mount_uploader :cover_image, ImageUploader
	mount_uploader :hero_image, ImageUploader

	def count_lessons
		Lesson.where(:course_id => self.id, :active => true).size()
	end
	
end
