class User < ActiveRecord::Base	

	COMPANY_ROLE = 'C'
	USER_ROLE = 'M' #Marketplace

	STANDARD_SMALL = 'SS'
	STANDARD_LARGE = 'SL'
	PREMIUM_SMALL = 'PS'
	PREMIUM_LARGE = 'PL'

	has_secure_password

	validates_presence_of :password, :on => :create
	validates_presence_of :role, :on => :create
	validates_uniqueness_of :email, :message => 'already used by other user'

	def send_password_reset
		generate_token(:password_reset_token)
		self.password_reset_sent_at = Time.zone.now		
		save!
		UserMailer.password_reset(self).deliver
	end

	def send_teacher_confirm(student_signup)
		generate_token(:confirm_email_token)
		self.confirm_email_sent_at = Time.zone.now
		save!
		UserMailer.teacher_confirm(self, student_signup).deliver
	end

	def send_company_welcome
		UserMailer.company_welcome(self).deliver
	end

	def generate_token(column)
		begin
			self[column] = SecureRandom.urlsafe_base64
		end while User.exists?(column => self[column])	
	end

	def full_name
		first + ' ' + last
	end

end
