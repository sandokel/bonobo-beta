class Lesson < ActiveRecord::Base
	
	VIDEO_LESSON = 'V'
	QUIZ_LESSON = 'Q'
	SLIDE_LESSON = 'S'

	belongs_to :course

	has_many :lesson_resources
	has_many :media_libraries, through: :lesson_resources
	has_many :lesson_slides
	accepts_nested_attributes_for :lesson_slides

	def lessonType
		if(self.lesson_type == VIDEO_LESSON)
			'Video'
		elsif(self.lesson_type == QUIZ_LESSON)
			'Quiz'
		else
			'Slides'
		end
	end
	
end
