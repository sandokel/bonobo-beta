class LessonsController < ApplicationController

	layout 'courses'

	def index
		@user = current_user
		@course = Course.find(params[:course_id])
		@lessons = Lesson.where(:course => @course, :active => true)
		@new_course_success = params[:new_course_success]
		@new_lesson_success = params[:new_lesson_success]
	end

	def destroy
		lesson = Lesson.find(params[:id])
		lesson.active = false
		lesson.save
		respond_to do |format|
			format.js { @lesson = lesson }
		end
	end

	def publish
		lesson = Lesson.find(params[:id])
		lesson.published = true
		lesson.save
		respond_to do |format|
			format.js { @lesson = lesson }
		end
	end

	def image
		@media_library = MediaLibrary.find(params[:id])
		render :layout => false 
	end
	
end
