class CoursesController < ApplicationController
	
	skip_before_filter  :verify_authenticity_token

	def index
		@user = current_user
		@courses = Course.where(:user_id => @user.id)
		render :layout => 'course_landing'
	end

	def new
		@course = Course.new()
		@user = current_user
	end

	def create
		@course = Course.create(course_params)
		@user = current_user
		redirect_to lessons_path(:course_id => @course.id, :new_course_success => true)
	end

	def update
		@course = Course.find(params[:id])
		@course.update_attributes(course_params)
		@course.save
		respond_to do |format|
			format.js {}
		end
	end

	def show
		@course = Course.find(params[:id])
		@user = current_user
		@lessons = Lesson.where(:course => @course, :active => true)
	end

	def syllabus
		
	end

	def publish
		course = Course.find(params[:id])
		@user = User.find(params[:userId])

		course.published = true
		course.save
		lessons = Lesson.where(:course => course)
		lessons.each do |lesson|
			lesson.published = true
			lesson.save
		end
		
		respond_to do |format|
			format.js { @updated_at = course.updated_at.strftime("%a %b %d %Y") }
		end
	end

	def settings
		@user = current_user
		@course = Course.find(params[:id])
		@lessons = Lesson.where(:course => @course, :active => true)
	end

	def unpublish
		@user = current_user
		@course = Course.find(params[:id])
		@course.published = false
		@course.save
		@lessons = Lesson.where(:course => @course, :active => true)
		@lessons.each do |lesson|
			lesson.published = false
			lesson.save
		end
		redirect_to course_settings_path(@course)
	end

	private
	    def course_params
	      params.require(:course).permit(:title, :description, :course_category_id, :course_level_id, :user_id, :cover_image, :hero_image)
	    end
	
end
