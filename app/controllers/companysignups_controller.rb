require 'json'

class CompanysignupsController < ApplicationController

	layout 'signup'

	def index
		if(!params[:plan])
			params[:plan] = 'standard-sm'
		end
		retrieve_stripe_plan
	end

	def new
		hand_over_params
		@user = User.new
	end

	def create
		print user_params
		@user = User.new(user_params)
		if @user.save
			redirect_to companysignup_path(@user, :price_int_part=>params[:price_int_part], :price_dec_part=>params[:price_dec_part], 
										  :plan=>params[:plan], :plan_title=>params[:plan_title], :users_number=>params[:users_number], :price=>params[:price])
  		else  			
  			hand_over_params
  			flash.now.notice = @user.errors.messages
  			render :new
  		end
	end

	def show
		hand_over_params
		@user = User.find(params[:id])
	end

	def update
		user = User.find(params[:id])		
		customer = create_stripe_customer		
		if customer && user.update_attribute(:stripe_id, customer.id)			
			hand_over_params
			user.send_company_welcome
		else
			flash.now.notice = 'Stripe customer can not be created'
			render :update
		end				
	end

	private
	    def user_params
	      params.require(:user).permit(:first, :last, :company, :job_title, :email, 
	      									:password, :password_confirmation, :role, :package_level)
	    end

	    def retrieve_stripe_plan
			plan = Stripe::Plan.retrieve(params[:plan])
			@price = (plan.amount.to_f/100).to_s
			@price = "%.2f" % @price
			price_split = @price.split('.')
			@price_int_part = price_split[0]
			@price_dec_part = price_split[1]
			@plan = params[:plan]		
			if @plan == 'standard-sm'
				@plan_title = 'Standard Small'
				@users_number = '99'
				@package_level = User::STANDARD_SMALL
			elsif @plan == 'standard-lg'
				@plan_title = 'Standard Large'
				@users_number = '250'
				@package_level = User::STANDARD_LARGE
			elsif @plan == 'premium-sm'
				@plan_title = 'Premium Small'
				@users_number = '99'
				@package_level = User::PREMIUM_SMALL
			else
				@plan_title = 'Premium Large'
				@users_number = '250'
				@package_level = User::PREMIUM_LARGE
			end	    	
	    end

	    def hand_over_params
	    	@price_int_part = params[:price_int_part]
			@price_dec_part = params[:price_dec_part]
			@token = params[:stripeToken]
			@plan = params[:plan]
			@plan_title = params[:plan_title]
			@users_number = params[:users_number]
			@name = params[:name]
			@price = params[:price]
			@package_level = params[:package_level]	
	    end

	    def create_stripe_customer
	    	customer = Stripe::Customer.create(
	  			:card => params[:stripeToken],
	  			:plan => params[:plan],
	  			:email => params[:email],
	  			:description => params[:name]
			)
	    end

end
