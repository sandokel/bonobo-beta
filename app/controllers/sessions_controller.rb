class SessionsController < ApplicationController

	def new
	end

	def create
		user = User.find_by_email(params[:email])		
		if user && user.authenticate(params[:password])
			session[:user_id] = user.id
			if !user.active
				user.active = true
				user.save
			end
			redirect_to courses_path
		else
			flash.now.alert = 'Invalid email or password'
			render 'new'
		end
	end

	def destroy
		session[:user_id] = nil
		redirect_to log_in_path, :notice => 'Logged out!'
	end

end
