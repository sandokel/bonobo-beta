class SignupsController < ApplicationController

	layout 'teacher_student_signup'

	def new
		@user = User.new()
		@show_sign_in = true
		@student_signup = params[:student_signup]
	end

	def create
		@user = User.new user_params
		if @user.save
			@user.send_teacher_confirm(params[:student_signup] == 'true')
		else			
			flash.now.notice = @user.errors.full_messages
			render :new
		end
	end

	def edit
		@user = User.find_by_confirm_email_token(params[:id])
		if(params[:student_signup] == 'true')
			session[:user_id] = @user.id
			render 'update'
		elsif 
			@show_sign_in = true
			if @user.confirm_email_sent_at < 7.days.ago || @user.stripe_id
				render :accessdenied			
			end
		end
	end

	def update
		user = User.find(params[:id])
		if(user.stripe_id)
			render :accessdenied
		elsif(params[:id] && params[:stripeToken])
			user = User.find(params[:id])
			recipient = Stripe::Recipient.create(
			  :name => params[:first] + ' ' + params[:last],
	  		  :type => "individual",
	  		  :email => user.email,
	  		  :bank_account => params[:stripeToken],
	  		  :email => user.email
			)		
			user.update_attribute(:stripe_id, recipient.id)
			session[:user_id] = user.id
		elsif params[:skip]
			session[:user_id] = user.id
		end		
	end

	private
	    def user_params
	      params.require(:user).permit(:first, :last, :email, :password, :password_confirmation, :role)
	    end

end
