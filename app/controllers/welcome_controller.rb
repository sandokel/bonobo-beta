class WelcomeController < ApplicationController
	
	layout 'application'

	def index
		@courses = Course.where(:published => true)
	end

end
