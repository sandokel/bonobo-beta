class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  helper_method :current_user

  before_action :authorize 

  private

  def current_user
  	@current_user ||= User.find(session[:user_id]) if session[:user_id]
  end

  def authorize
  	if current_user
  		return true
  	elsif ['welcome','companysignups', 'signups', 'sessions'].include? params[:controller]
  		return true
  	else
  		redirect_to(:controller => 'welcome')
  	end
  end

end
