class VideolessonsController < ApplicationController

	layout 'courses'

	def new
		@user = current_user
		@lesson = Lesson.new
		@course = Course.find(params[:course_id])
		@lessons = Lesson.where(:course => @course, :active => true)
		@resources = MediaLibrary.where(:user_id => @user.id, :link => nil)
		@links = MediaLibrary.where(:user_id => @user.id, :file_name => nil)
	end

	def create
		@lesson = Lesson.create(lesson_params)
		redirect_to lessons_path(:course_id => @lesson.course.id, :new_lesson_success => true)
	end

	def update
		@lesson = Lesson.find(params[:id])
		@lesson.update_attributes(lesson_params)
		@lesson.save
		respond_to do |format|
			format.js { @course = @lesson.course }
		end
	end

	def show
		@lesson = Lesson.find(params[:id])
		@course = @lesson.course
		if @course.published
			@lessons = Lesson.where(:course => @course, :active => true, :published => true)
		else
			@lessons = Lesson.where(:course => @course, :active => true)
		end
		@user = current_user
		@lesson_number = @lessons.index(@lesson)
		next_lesson_index = @lesson_number + 1
		if next_lesson_index >= @lessons.length
			@next_lesson = @lessons.first
		else
			@next_lesson = @lessons[next_lesson_index]
		end
		@lesson_resources = LessonResource.where(:lesson => @lesson)
		render :layout => 'lessons'
	end

	def edit
		@lesson = Lesson.find(params[:id])
		@course = @lesson.course
		@lessons = Lesson.where(:course => @course, :active => true)
		@user = current_user
		@resources = MediaLibrary.where(:user_id => @user.id, :link => nil)
		@links = MediaLibrary.where(:user_id => @user.id, :file_name => nil)
		lesson_resources = LessonResource.where(:lesson => @lesson)
		@lesson_media_resources = lesson_resources.map { |resource| resource.media_library }
	end

	private
	    def lesson_params
	      params.require(:lesson).permit(:title, :description, :lesson_type, :order, :course_id, :vimeo_code, :transcript, :published, :media_library_ids => [])
	    end
end
