class SlideLessonsController < ApplicationController
	
	layout 'courses'

	def new
		@user = current_user
		@course = Course.find(params[:course_id])
		@lesson = Lesson.new
		30.times do |i|
			if i == 0
				slide = LessonSlide.new(:order => i+1, :active => true)
			else
				slide = LessonSlide.new(:order => i+1, :active => false)
			end
			@lesson.lesson_slides << slide
		end
	end

	def create
		lesson = Lesson.new(lesson_params)
		lesson.save
		redirect_to lessons_path(:course_id => lesson[:course_id])
	end

	def edit
		@user = current_user
		@lesson = Lesson.find(params[:id])
		@lesson.lesson_slides = @lesson.lesson_slides.select { |x| x.active }
		@not_used_slides = []
		(@lesson.lesson_slides.length..30).each do |i|
			slide = LessonSlide.new(:order => i+1, :active => false)
			@not_used_slides << slide
		end
		@course = Course.find(@lesson.course_id)
		@edit = true
	end

	def update
		@lesson = Lesson.find(params[:id])
		@lesson.update_attributes(lesson_params)
		@lesson.save
		redirect_to edit_slide_lesson_path(@lesson)
	end

	private
	    def lesson_params
	      params.require(:lesson).permit(:title, :description, :lesson_type, :order, :course_id, :published, :active, :lesson_slides_attributes => [:id, :title, :description, :order, :css_position, :image, :active, :lesson_id], :media_library_ids => [])
	    end
	
end
