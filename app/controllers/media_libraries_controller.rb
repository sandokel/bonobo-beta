class MediaLibrariesController < ApplicationController

	def create
		if(params[:file_name])
			@media = MediaLibrary.create(:file_name => params[:file_name], :user_id => params[:user_id])
		else
			@media = MediaLibrary.create(:link => params[:link], :user_id => params[:user_id])
		end
		respond_to do |format|
			format.js {}
		end
	end

end
