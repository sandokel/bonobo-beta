class UserMailer < ActionMailer::Base
  default from: 'support@bonobolearning.com'

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_mailer.password_reset.subject
  #
  def password_reset(user)
    @user = user
    mail(to: user.email, subject:'Password Reset')
  end

  def teacher_confirm(user, student_signup)
     @user = user
     @student_signup = student_signup
     mail(to:user.email, subject:'Welcome to Bonobo')
  end

  def company_welcome(user)
     @user = user
     mail(to: @user.email, subject: 'Welcome to Bonobo')     
  end
  
end
