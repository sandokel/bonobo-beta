require 'spec_helper'

describe "LogIns" do
  
  fixtures :users

  before(:each) do
    assert_equal 1, User.count()
  	existing_user = User.find(1)
  	assert_equal nil, existing_user.active
  	assert_equal 'example@email.com', existing_user.email
  	visit log_in_path
  	fill_in "email", :with => existing_user.email
  end
  
  it "log in with success" do
  	fill_in "password", :with => 'Testing8'
  	click_button "Sign In"
  	page.should have_content('Jakub Sokolowski')
  	assert User.find(1).active
  end

  it "log in with failure" do
  	fill_in "password", :with => 'Testing'
  	click_button "Sign In"
  	page.should have_content('Invalid email or password')
  	assert_equal nil, User.find(1).active
  end
  
end
