require 'spec_helper'

describe "CreateCourses" do

  fixtures :users, :course_categories, :course_levels
  
  before(:each) do
  	existing_user = User.find(1)
  	visit log_in_path
  	fill_in "email", :with => existing_user.email
  	fill_in "password", :with => 'Testing8'
  	click_button "Sign In"
  	expect(current_path).to eq(courses_path)
  end

  it "create course" do
  	visit new_course_path
    page.should have_content('Course Details')
    fill_in 'course_title', :with => 'jquery basic course'
    fill_in 'course_description', :with => 'for beginners'
    select('art', :from => 'course_course_category_id')
    select('beginner', :from => 'course_course_level_id')
    attach_file('course_cover_image', "#{Rails.root}/spec/fixtures/files/Jellyfish.jpg")
    attach_file('course_hero_image', "#{Rails.root}/spec/fixtures/files/Penguins.jpg")
    click_button "Next"
    page.should have_content('Course Syllabus')
    page.should have_content('jquery basic course')
    assert_equal 1, Course.count
    course = Course.find(1)
    assert_equal 'jquery basic course', course.title
    assert_equal 'for beginners', course.description
    category = CourseCategory.find(1)
    level = CourseLevel.find(1)
    assert_equal 1, course.course_category_id
    assert_equal 1, course.course_level_id
    assert_equal "Jellyfish.jpg", course.cover_image.file.filename
    assert_equal "Penguins.jpg", course.hero_image.file.filename
  end

end
