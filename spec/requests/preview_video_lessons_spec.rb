require 'spec_helper'

describe "PreviewVideoLessons" do

  fixtures :users, :course_categories, :course_levels, :courses, :lessons, :media_libraries, :lesson_resources 
 
  let(:course) { Course.find(1) }

  before(:each) do
  	existing_user = User.find(1)
  	visit log_in_path
  	fill_in "email", :with => existing_user.email
  	fill_in "password", :with => 'Testing8'
  	click_button "Sign In"
  	expect(current_path).to eq(courses_path)
  end

  it "check lesson resources" do
    assert_equal 4, Lesson.where(:course => course).count
  	visit lessons_path(:course_id => course.id)
  	click_on 'Preview'
  	page.should have_content('game dev lesson')
  	page.should have_content('game programming')
  	page.should have_content('Heart Like a Rabbit')
  	page.should have_content('fez.jpg')
  	page.should have_content('http://www.polygamia.pl')
    page.should have_no_content('angular lesson')
    click_on 'Next'
    page.should have_content('java script lesson')
    page.should have_content('js programming')
    page.should have_content('Washed Out - Weightless')
    page.should have_content('watch_dogs.jpg')
    page.should have_content('http://www.tvgry.pl')
    page.should have_no_content('angular lesson')
    click_on 'Next'
    page.should have_content('database lesson')
    page.should have_content('using sql')
    page.should have_content('Keep On Pushin')
    page.should have_content('witcher.jpg')
    page.should have_content('http://www.inputlag.pl')
    page.should have_no_content('angular lesson')
    click_on 'Next'
    page.should have_content('game dev lesson')
    page.should have_content('game programming')
    page.should have_content('Heart Like a Rabbit')
    page.should have_content('fez.jpg')
    page.should have_content('http://www.polygamia.pl')
    page.should have_no_content('angular lesson')
  end

end
