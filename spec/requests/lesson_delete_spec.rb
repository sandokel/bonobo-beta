require 'spec_helper'

describe "LessonDelete", :js => true do

  fixtures :users, :course_categories, :course_levels, :courses, :lessons
  
  before(:each) do
  	existing_user = User.find(1)
  	visit log_in_path
  	fill_in "email", :with => existing_user.email
  	fill_in "password", :with => 'Testing8'
  	click_button "Sign In"
  	expect(current_path).to eq(courses_path)
  end

  it "delete lesson" do
  	page.find("#course_1").click
    page.find('#syllabus').click
    page.should have_content('game dev lesson')
    page.find('#lesson-remove-1').click
    page.driver.browser.switch_to.alert.dismiss
    page.should have_content('game dev lesson')
    page.find('#lesson-remove-1').click
    page.driver.browser.switch_to.alert.accept
    within '.success' do
      page.should have_content('Lesson has been removed')
    end
    page.should have_no_content('game dev lesson')
  end

end
