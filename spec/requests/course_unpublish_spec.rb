require 'spec_helper'

describe "CourseUnpublish", :js => true do

  fixtures :users, :course_categories, :course_levels, :courses, :lessons
  
  before(:each) do
  	existing_user = User.find(1)
  	visit log_in_path
  	fill_in "email", :with => existing_user.email
  	fill_in "password", :with => 'Testing8'
  	click_button "Sign In"
  	expect(current_path).to eq(courses_path)
  end

  it "unpublish course" do
  	page.find("#course_1").click
    page.find('#syllabus').click
    page.should have_content('Live')
    click_on 'Settings'
    page.find('#unpublish').click
    page.should have_content('Unpublishing this class will remove it from the Bonobo Marketplace. Current and past students will have access to the class for 60 days before their access is removed.')
    click_on 'Cancel'
    page.should have_content('Unpublish')
    page.find('#unpublish').click
    page.find('#continue').click
    page.should have_content('Unpublished')
    page.should have_content('Publish')
    page.find('#syllabus').click
    page.should have_content('Draft')
    page.find('#publish-course').click
    click_on 'Settings'
    page.should have_content('Published')
    page.should have_content('Unpublish')
  end

end
