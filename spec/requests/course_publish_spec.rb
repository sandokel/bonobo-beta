require 'spec_helper'

describe "CoursePublish", js:true do

  fixtures :users, :course_categories, :course_levels
  
  before(:each) do
  	existing_user = User.find(1)
  	visit log_in_path
  	fill_in "email", :with => existing_user.email
  	fill_in "password", :with => 'Testing8'
  	click_button "Sign In"
  	expect(current_path).to eq(courses_path)
  end

  it "publish course with update" do
  	
    create_course

    create_video_lesson('Lesson 1')
    publish
    check_alert_about_too_less_lessons

    create_video_lesson('Lesson 2')
    publish
    check_alert_about_too_less_lessons

    create_video_lesson('Lesson 3')
    publish
    check_course_published_alert

    edit_lesson
    page.should have_content('Video lesson has been updated')
    page.find('#syllabus').click 
    page.should have_content('Lesson 1 updated')

    create_video_lesson('Lesson 4')
    do_not_publish_new_lesson
    page.all("#publish_lesson").count.should eql(1)
    
    create_video_lesson('Lesson 5')
    do_not_publish_new_lesson
    page.all("#publish_lesson").count.should eql(2)

    create_video_lesson('Lesson 6')
    publish_new_lesson
    page.all("#publish_lesson").count.should eql(2)
    
    first("#publish_lesson").click
    check_lesson_published_alert
    page.all("#publish_lesson").count.should eql(1)
  end

  def create_course
    visit new_course_path
    page.should have_content('Course Details')
    fill_in 'course_title', :with => 'jquery basic course'
    fill_in 'course_description', :with => 'for beginners'
    select('art', :from => 'course_course_category_id')
    select('beginner', :from => 'course_course_level_id')
    attach_file('course_cover_image', "#{Rails.root}/spec/fixtures/files/Jellyfish.jpg")
    attach_file('course_hero_image', "#{Rails.root}/spec/fixtures/files/Penguins.jpg")
    click_button "Next"
  end

  def create_video_lesson(lesson_title)
    page.should have_content('Course Syllabus')
    click_link "Video"
    page.should have_content('Video')
    fill_in 'lesson_title', :with => lesson_title
    fill_in 'lesson_description', :with => 'Sample description'
    fill_in 'lesson_vimeo_code', :with => '<iframe src="//player.vimeo.com/video/65926401" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> <p><a href="http://vimeo.com/65926401">Heart Like a Rabbit</a> from <a href="http://vimeo.com/anthonyschepperd">Anthony Francisco Schepperd</a> on <a href="https://vimeo.com">Vimeo</a>.</p>'
    fill_in 'lesson_transcript', :with => 'movie transcript'
    page.find("#addNewLesson").click
  end

  def check_alert_about_too_less_lessons
    page.should have_content('You must have at least 3 lessons to publish your course. If you would like to preview your course click Preview')
  end

  def do_not_publish_new_lesson
    page.should have_content('Would you like to publish this new lesson to your class now?')
    page.should have_content('Later')
    page.should have_content('Publish Now')
    page.find("#later").click
  end

  def publish
    page.find(".publish").click
  end

  def publish_new_lesson
    page.should have_content('Would you like to publish this new lesson to your class now?')
    page.find("#publishNow").click
  end

  def check_course_published_alert
      within '.success' do
        page.should have_content('Course has been published')
      end
  end

  def check_lesson_published_alert
      page.should have_content('Lesson has been published')
  end

  def edit_lesson
    click_on 'Lesson 1'
    fill_in 'lesson_title', :with => 'Lesson 1 updated'
    fill_in 'lesson_description', :with => 'Sample description updated'
    page.find('.primary').click #click Update button
    page.should have_content('Updating this lesson will auotmatically publish and overide the lesson for all your current students.')
    page.find('#continue').click #click Continue button
  end

end
