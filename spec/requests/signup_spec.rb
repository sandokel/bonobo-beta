require 'spec_helper'

describe "Signup", js:true do
  
  it "teacher sign up with bank account" do
  	signup_common_part(false)
    page.should have_content('Create & Sell Classes for Free!')
    page.should have_content('Tell us where to deliver the money you earn from the sales of your classes.')
    fill_in 'first', :with => 'teacher'
    fill_in 'last', :with => 'firsth'
    select('Checking', :from => 'account_type')
    fill_in 'account_number', :with => '000123456789'
    fill_in 'account_number_confirmation', :with => '000123456781'
    fill_in 'routing_number', :with => '110000000'
    fill_in 'routing_number_confirmation', :with => '110000001'
    click_on 'Continue'
    page.should have_content('Account number doesn\'t match')
    page.should have_content('Routing number doesn\'t match')
    fill_in 'account_number', :with => '000123456789'
    fill_in 'account_number_confirmation', :with => '000123456789'
    fill_in 'routing_number', :with => '110000000'
    fill_in 'routing_number_confirmation', :with => '110000000'
    click_on 'Continue'
    page.should have_content('Congratulations!')
    page.should have_content('Your Bonobo account is ready. Let\'s start learning.')
    user = User.find(1)
    assert_not_nil user.stripe_id
    assert_equal 'M', user.role
    click_on 'Get Started'
    page.should have_content('My Courses')
  end

  it "teacher signup without bank account" do
    signup_common_part(false)
    page.should have_content('Create & Sell Classes for Free!')
    page.should have_content('Tell us where to deliver the money you earn from the sales of your classes.')
    click_on "Skip this step"
    page.should have_content('Congratulations!')
    page.should have_content('Your Bonobo account is ready. Let\'s start learning.')
    user = User.find(1)
    assert_nil user.stripe_id
    assert_equal 'M', user.role
    click_on 'Get Started'
    page.should have_content('My Courses')
  end

  it "student sign up" do
    signup_common_part(true)
    page.should have_content('Congratulations!')
    page.should have_content('Your Bonobo account is ready. Let\'s start learning.')
    user = User.find(1)
    assert_nil user.stripe_id
    assert_equal 'M', user.role
    click_on 'Get Started'
    page.should have_content('My Courses')
  end

  def signup_common_part(student_signup)
    visit new_signup_path(:student_signup => student_signup)
    page.should have_content('CREATE AN ACCOUNT')
    page.should have_content('Buy & Sell Online Classes')
    click_on 'Continue'
    page.should have_content('Email is required.')
    page.should have_content('First name is required.')
    page.should have_content('Last name is required.')
    page.should have_content('Passwords must be at least 8 characters, one number or special character.')
    page.should have_content('Passwords must match.')
    fill_in 'user_email', :with => 'teacher1@xyz.com'
    fill_in 'user_first', :with => 'teacher'
    fill_in 'user_last', :with => 'first'
    fill_in 'user_password', :with => 'bonobo123'
    fill_in 'user_password_confirmation', :with => 'bonobo122'
    click_on 'Continue'
    page.should have_content('Passwords must match.')
    fill_in 'user_password_confirmation', :with => 'bonobo123'
    click_on 'Continue'
    page.should have_content('Almost there!')
    page.should have_content('Next check your email for a link to complete your sign-up process.')
    user = User.find(1)
    assert_equal 'teacher1@xyz.com', user.email
    assert_equal 'teacher', user.first
    assert_equal 'first', user.last
    visit edit_signup_path(id:user.confirm_email_token, student_signup:student_signup)
  end
  
end
