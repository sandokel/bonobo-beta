require 'spec_helper'

describe "CreateVideoLessons", :js => true do

  fixtures :users, :course_categories, :course_levels, :courses

  let(:course) { Course.find(2) }

  before(:each) do
  	existing_user = User.find(1)
  	visit log_in_path
  	fill_in "email", :with => existing_user.email
  	fill_in "password", :with => 'Testing8'
  	click_button "Sign In"
  	expect(current_path).to eq(courses_path)
  end

  def create_video_lesson_helper
  	visit lessons_path(:course_id => course.id)
  	page.should have_content('Course Syllabus')
  	click_link "Video"
    within '.video-lesson-title' do
  	 page.should have_content('Video Lesson')
    end
  	fill_in 'lesson_title', :with => 'Hello World'
  	fill_in 'lesson_description', :with => 'First App in Android'
  	fill_in 'lesson_vimeo_code', :with => '<iframe src="//player.vimeo.com/video/65926401" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> <p><a href="http://vimeo.com/65926401">Heart Like a Rabbit</a> from <a href="http://vimeo.com/anthonyschepperd">Anthony Francisco Schepperd</a> on <a href="https://vimeo.com">Vimeo</a>.</p>'
  	fill_in 'lesson_transcript', :with => 'movie transcript'
  end

  it "create video course" do
    create_video_lesson_helper
    page.find('#addNewLesson').click
  	assert_equal 1, Lesson.count()
  	lesson = Lesson.find(1)
  	assert_equal 'Hello World', lesson.title
  	assert_equal 'First App in Android', lesson.description
  	assert_equal '<iframe src="//player.vimeo.com/video/65926401" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> <p><a href="http://vimeo.com/65926401">Heart Like a Rabbit</a> from <a href="http://vimeo.com/anthonyschepperd">Anthony Francisco Schepperd</a> on <a href="https://vimeo.com">Vimeo</a>.</p>', lesson.vimeo_code
  	assert_equal 'movie transcript', lesson.transcript
  	assert_equal course.id, lesson.course_id
  	assert_equal Lesson::VIDEO_LESSON, lesson.lesson_type
  	assert_equal true, lesson.active
    page.should have_content('Hello World')
    click_on 'Preview'
    new_window=page.driver.browser.window_handles.last 
    page.within_window new_window do
      page.should have_content('Hello World')
      page.should have_content('First App in Android')
      page.should have_content('movie transcript')
      page.should have_content('Heart Like a Rabbit')
    end
  end

end
