﻿select * from courses;
select * from course_categories;
select * from lessons;
select * from lesson_slides;
select * from lessons where course_id=352;
select * from users;
select * from lesson_resources;
select * from media_libraries;
select * from lesson_slides;

update lessons set published=false;
update courses set updated_at='2014-08-31 12:12:12.11111';

truncate courses;
truncate lessons;
truncate lesson_resources;
truncate media_libraries;
truncate users;