# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150108211752) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "course_categories", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "course_levels", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "courses", force: true do |t|
    t.string   "title"
    t.string   "description"
    t.integer  "course_category_id"
    t.integer  "course_level_id"
    t.string   "cover_image"
    t.string   "hero_image"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
    t.boolean  "published"
  end

  create_table "lesson_resources", force: true do |t|
    t.integer  "lesson_id"
    t.integer  "media_library_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "lesson_slides", force: true do |t|
    t.string   "title"
    t.text     "description"
    t.string   "image"
    t.string   "css_position", limit: 8
    t.integer  "order"
    t.integer  "lesson_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "active",                 default: true
  end

  create_table "lessons", force: true do |t|
    t.string   "title",       limit: 512
    t.text     "description"
    t.string   "lesson_type", limit: 1
    t.integer  "course_id"
    t.integer  "order"
    t.text     "vimeo_code"
    t.text     "transcript"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "active",                  default: true
    t.boolean  "published"
  end

  create_table "media_libraries", force: true do |t|
    t.integer  "user_id"
    t.string   "file_name"
    t.string   "link"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "email",                  limit: 64
    t.string   "first",                  limit: 64
    t.string   "last",                   limit: 64
    t.string   "company",                limit: 128
    t.string   "phone",                  limit: 10
    t.string   "package_level",          limit: 2
    t.boolean  "active"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "job_title"
    t.string   "personal_title",         limit: 4
    t.string   "stripe_id",              limit: 128
    t.string   "password_digest"
    t.string   "password_reset_token"
    t.datetime "password_reset_sent_at"
    t.string   "confirm_email_token"
    t.datetime "confirm_email_sent_at"
    t.string   "role",                   limit: 1
  end

end
