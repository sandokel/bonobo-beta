class AddConfirmEmailTokenAndConfirmEmailSentAtToUsers < ActiveRecord::Migration
  def change
    add_column :users, :confirm_email_token, :string
    add_column :users, :confirm_email_sent_at, :datetime
  end
end
