class CreateLessonSlides < ActiveRecord::Migration
  def change
    create_table :lesson_slides do |t|
      t.string :title
      t.text :description
      t.string :image
      t.string :css_position, :limit => 1
      t.integer :order
      t.integer :lesson_id

      t.timestamps
    end
  end
end
