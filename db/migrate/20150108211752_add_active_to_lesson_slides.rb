class AddActiveToLessonSlides < ActiveRecord::Migration
  def change
  	add_column :lesson_slides, :active, :boolean, :default => true
  end
end
