class DeleteRegisteredColumnInUsers < ActiveRecord::Migration
  def change
  	remove_column :users, :registered
  end
end
