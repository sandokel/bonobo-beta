class CreateMediaLibraries < ActiveRecord::Migration
  def change
    create_table :media_libraries do |t|
      t.integer :user_id
      t.string :file_name
      t.string :page

      t.timestamps
    end
  end
end
