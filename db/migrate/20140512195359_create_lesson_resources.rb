class CreateLessonResources < ActiveRecord::Migration
  def change
    create_table :lesson_resources do |t|
      t.integer :lesson_id
      t.integer :media_library_id

      t.timestamps
    end
  end
end
