class CreateLessons < ActiveRecord::Migration
  def change
    create_table :lessons do |t|
      t.string :title, limit: 512
      t.text :description
      t.string :type, limit: 1
      t.integer :course_id
      t.integer :order
      t.text :vimeo_code
      t.text :transcript

      t.timestamps
    end
  end
end
