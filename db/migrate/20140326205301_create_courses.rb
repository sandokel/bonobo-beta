class CreateCourses < ActiveRecord::Migration
  def change
    create_table :courses do |t|
      t.string :title
      t.string :description
      t.integer :category_id
      t.integer :level_id
      t.string :cover_image
      t.string :hero_image

      t.timestamps
    end
  end
end
