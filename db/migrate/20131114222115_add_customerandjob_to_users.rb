class AddCustomerandjobToUsers < ActiveRecord::Migration
  def change
    add_column :users, :stripe_customer, :string
    add_column :users, :job_title, :string
  end
end
