class AddToLessonsActive < ActiveRecord::Migration
  def change
  	add_column :lessons, :active, :boolean, :default => true
  end
end
