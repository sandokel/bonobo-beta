class OneStripeColumnInUsers < ActiveRecord::Migration
  def change
  	remove_column :users, :stripe_customer, :string
  	remove_column :users, :stripe_recipient, :string
  	add_column :users, :stripe_id, :string, :limit => 128
  end
end
