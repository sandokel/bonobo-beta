class RenamteColumnsInCourses < ActiveRecord::Migration
  def change
  	rename_column :courses, :category_id, :course_category_id
  	rename_column :courses, :level_id, :course_level_id
  end
end
