class AddStripeRecipientToUsers < ActiveRecord::Migration
  def change
  	add_column :users, :stripe_recipient, :string, :limit => 128
  	change_column :users, :stripe_customer, :string, :limit => 128
  end
end
