class AddPublishToLessons < ActiveRecord::Migration
  def change
    add_column :lessons, :published, :Boolean
  end
end
