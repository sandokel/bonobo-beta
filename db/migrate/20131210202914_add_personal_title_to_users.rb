class AddPersonalTitleToUsers < ActiveRecord::Migration
  def change
  	add_column :users, :personal_title, :string, :limit => 4
  end
end
