class ChangePageColumnNameInMediaLibraries < ActiveRecord::Migration
  def change
  	rename_column :media_libraries, :page, :link
  end
end
