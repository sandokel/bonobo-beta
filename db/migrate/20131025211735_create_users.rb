class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :email, limit: 64
      t.string :password, limit: 64
      t.string :first, limit: 64
      t.string :last, limit: 64
      t.string :company, limit: 128
      t.string :phone, limit: 10
      t.string :package_level, limit: 1
      t.boolean :registered
      t.boolean :active

      t.timestamps
    end
  end
end
