class CompanyAndJobtitleOptional < ActiveRecord::Migration
  def change
  	change_column :users, :company, :string, :limit => 128, :null => true 
    change_column :users, :job_title, :string, :limit => 255, :null => true
  end
end
