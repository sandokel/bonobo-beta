class ChangeSlideLessonCssPositionColumn < ActiveRecord::Migration
  def change
  	change_column :lesson_slides, :css_position, :string, :limit => 8
  end
end
