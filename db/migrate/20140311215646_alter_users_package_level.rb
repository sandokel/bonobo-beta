class AlterUsersPackageLevel < ActiveRecord::Migration
  def change
  	change_column :users, :package_level, :string, :limit => 2, :null => true 
  end
end
