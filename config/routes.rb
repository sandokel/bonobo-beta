BonoboBeta::Application.routes.draw do    
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'welcome#index'

  resources :companysignups

  resources :signups

  get 'login' => 'sessions#new', :as => 'log_in'
  get 'logout' => 'sessions#destroy', :as => 'log_out'
  resources :sessions

  get 'resetpassword' => 'password_resets#new', :as => 'resetpassword'
  get 'changepassword/:id' => 'password_resets#edit', :as => 'changepassword'
  resources :password_resets

  resources :courses
  match '/course/publish/:id' => 'courses#publish', :as => 'course_publish', via: [:get, :post]
  get '/course/settings/:id' => 'courses#settings', :as => 'course_settings'
  post '/course/unpublish/:id' => 'courses#unpublish', :as => 'course_unpublish'

  resources :lessons
  get 'lesson/image/:id' => 'lessons#image', :as => 'lesson_image'
  get 'lesson/publish/:id' => 'lessons#publish', :as => 'lesson_publish'

  resources :videolessons

  resources :media_libraries

  resources :users
  get 'users' => 'users#edit', :as => 'users_edit'
  match '/users/account/' => 'users#account', :as => 'users_bank', via: [:get, :post]

  resources :slide_lessons
  
  #resources :companysignup

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end
  
  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
