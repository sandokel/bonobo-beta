namespace :db do
	desc "Init course levels and categories"
	task :populate => :environment do
		[CourseLevel, CourseCategory].each(&:delete_all)

		CourseLevel.create(id:1, name:'Beginner')
		CourseLevel.create(id:2, name:'Intermediate')
		CourseLevel.create(id:3, name:'Advanced')

		CourseCategory.create(id:1, name:'Academics')
		CourseCategory.create(id:2, name:'Art')
		CourseCategory.create(id:3, name:'Bussiness')
		CourseCategory.create(id:4, name:'Crafts')
		CourseCategory.create(id:5, name:'Design')
		CourseCategory.create(id:6, name:'Engineering')
		CourseCategory.create(id:7, name:'Food')
		CourseCategory.create(id:8, name:'IT Software')
		CourseCategory.create(id:9, name:'Languages')
		CourseCategory.create(id:10, name:'Music')
	end
end